<?php

function surline($text, $status) {
 $out = "";
 switch($status) {
  case "SURLINE":
   $out = "[43m"; 
   break;

  default:
   throw new Exception("Invalid status: " . $status);
 }
 return chr(27) . "$out" . "$text" . chr(27) . "[0m";
}
if($argc > 1)
{
	$end = 0;
	$stdin = fopen("php://stdin", 'r');
	$entrer = fgets($stdin);
	$explode = explode('\n', $entrer);
	$i = 0;
	foreach ($explode as $key => $word)
	{
		$len = strlen($word) -1;
		$file_name = $argv[1];
		$compteur = 0;
		if($file = file_get_contents($file_name))
		{
			for($x=0;$x<strlen($file);$x++)
			{
				if($compteur < $len && $file[$x] == $word[$compteur])
				{
					if($word[$compteur] == $word[$len])
					{
						echo surline($file[$x],"SURLINE");
						$i++;
					}
					else
						echo surline($file[$x],"SURLINE");
					$compteur++;
				} 
				else
					echo $file[$x];
			}
		}	
	}
	exit($i);
}	
?>
